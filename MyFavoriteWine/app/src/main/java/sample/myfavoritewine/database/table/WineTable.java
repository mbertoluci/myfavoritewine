package sample.myfavoritewine.database.table;

/**
 * Created by Monan on 10/26/15.
 */
public interface WineTable {

    String TABLE_NAME = "wine";

    String ID = "id";
    String NAME = "name";
    String PICTURE = "picture";
    String PRODUCTOR = "productor";
    String ORIGIN = "origin";
    String DISTRIBUTOR = "distributor";
    String IMPORTER = "importer";
    String PRICE = "price";

    String[] ALL_COLUMNS = new String[]{ID, NAME, PICTURE, PRODUCTOR, ORIGIN, DISTRIBUTOR, IMPORTER, PRICE};

    String SQL_CREATE = "CREATE TABLE " + TABLE_NAME + " (" + ID + " INTEGER PRIMARY KEY AUTOINCREMENT " + ","
                                                            + NAME          + " TEXT " + ","
                                                            + PICTURE       + " TEXT " + ","
                                                            + PRODUCTOR     + " TEXT " + ","
                                                            + ORIGIN        + " TEXT " + ","
                                                            + DISTRIBUTOR   + " TEXT " + ","
                                                            + IMPORTER      + " TEXT " + ","
                                                            + PRICE         + " TEXT " + ")";

    String SQL_DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;
}
