package sample.myfavoritewine.database.helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import sample.myfavoritewine.database.table.WineTable;
import sample.myfavoritewine.service.model.Wine;

/**
 * Created by Monan on 10/26/15.
 */
public class WineHelper extends BaseHelper{

    public WineHelper(Context context) {
        super(context);
    }

    public boolean insertFromWineList(List<Wine> wineList){

        List<Long> idList = new ArrayList<Long>();
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        for(Wine wine : wineList){
            ContentValues values = new ContentValues();
            values.put(WineTable.NAME, wine.getName());
            values.put(WineTable.PICTURE, wine.getPicture());
            values.put(WineTable.PRODUCTOR, wine.getProductor());
            values.put(WineTable.ORIGIN, wine.getOrigin());
            values.put(WineTable.DISTRIBUTOR, wine.getDistributor());
            values.put(WineTable.IMPORTER, wine.getImporter());
            values.put(WineTable.PRICE, wine.getPrice());

            long id = db.insert(WineTable.TABLE_NAME, null, values);
            idList.add(id);
        }

        boolean result = false;
        if (wineList.size() == idList.size()){
            result = true;
        }

        return result;
    }

    public boolean hasDataOnDatabase(){
        String sql = "SELECT " + WineTable.ID +  " FROM " + WineTable.TABLE_NAME;
        SQLiteDatabase db = databaseHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery(sql, null);
        return (cursor.getCount() > 0);
    }

    public List<Wine> getWineList(){
        String sql = "SELECT * FROM " + WineTable.TABLE_NAME;
        SQLiteDatabase db = databaseHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery(sql, null);

        List<Wine> result = new ArrayList<Wine>();
        if (cursor.moveToFirst()){
            do{
                Wine wine = new Wine(cursor);
                result.add(wine);
            }while(cursor.moveToNext());
        }
        return result;
    }

    public void clearWineList(){
        SQLiteDatabase db = databaseHelper.getReadableDatabase();
        db.delete(WineTable.TABLE_NAME, null, null);
    }

}
