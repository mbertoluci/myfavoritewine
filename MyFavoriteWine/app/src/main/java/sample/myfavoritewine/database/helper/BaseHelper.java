package sample.myfavoritewine.database.helper;

import android.content.Context;

import sample.myfavoritewine.database.DatabaseHelper;

/**
 * Created by Monan on 10/26/15.
 */
public abstract class BaseHelper {
    protected DatabaseHelper databaseHelper;

    public BaseHelper(Context context){
        databaseHelper = new DatabaseHelper(context);
    }

}
