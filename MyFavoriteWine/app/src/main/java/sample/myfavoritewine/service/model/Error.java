package sample.myfavoritewine.service.model;

/**
 * Created by Monan on 9/5/15.
 */
public class Error{

    public static final int NO_INTERNET_AVAILABLE = 1001;

    public int errorCode;
    public String errorMessage;

}
