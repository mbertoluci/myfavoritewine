package sample.myfavoritewine.service.business;

import android.content.Context;

import java.util.List;

import sample.myfavoritewine.database.helper.WineHelper;
import sample.myfavoritewine.service.model.OperationalResult;
import sample.myfavoritewine.service.model.User;
import sample.myfavoritewine.service.model.UserResponse;
import sample.myfavoritewine.service.model.Wine;
import sample.myfavoritewine.service.model.WineResponse;

/**
 * Created by Monan on 9/8/15.
 */
public class WineBusiness extends BaseBusiness{

    public WineBusiness(){
        super();
    }

    public OperationalResult<WineResponse> getWineList(Context context){

        OperationalResult<WineResponse> result = new OperationalResult<WineResponse>();

        List<Wine> response;
        WineHelper wineHelper = new WineHelper(context);
        if(wineHelper.hasDataOnDatabase()){
            response = wineHelper.getWineList();
        }else{
            response = mMongolabManager.getWineList();
            wineHelper.insertFromWineList(response);
        }

        if (response.size() > 0){
            result.operationalResultResponseType = OperationalResult.OperationalResultResponseType.SUCCESS;
            WineResponse wineResponse = new WineResponse();
            wineResponse.setWineList(response);
            wineResponse.setRequestAt();
            result.result = wineResponse;

        }else{
            result.operationalResultResponseType = OperationalResult.OperationalResultResponseType.ERROR;
        }

        return result;
    }
}
