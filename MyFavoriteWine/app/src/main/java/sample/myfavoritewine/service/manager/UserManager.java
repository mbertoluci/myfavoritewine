package sample.myfavoritewine.service.manager;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import sample.myfavoritewine.R;
import sample.myfavoritewine.service.business.UserBusiness;
import sample.myfavoritewine.service.listener.OperationalListener;
import sample.myfavoritewine.service.model.*;
import sample.myfavoritewine.service.model.Error;
import sample.myfavoritewine.service.util.NetworkUtil;

/**
 * Created by Monan on 9/5/15.
 */
public class UserManager extends BaseManager{

    private Context mContext;

    public UserManager(Context context) {
        super(context);
        mContext = context;
    }

    public UserManager(Context context, CharSequence loadingMessage) {
        super(context, loadingMessage);
        mContext = context;
    }

    public void register(final User user, final OperationalListener<UserResponse> callback){

        if(!NetworkUtil.isConnectionAvailable(mContext)){
            sample.myfavoritewine.service.model.Error error = new Error();
            error.errorMessage = mContext.getResources().getString(R.string.label_problem_with_connection);
            error.errorCode = Error.NO_INTERNET_AVAILABLE;
            callback.onOperationError(null, error);
            return;
        }

        AsyncTask<User, Void, OperationalResult<UserResponse>> asyncTask = new AsyncTask<User, Void, OperationalResult<UserResponse>>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                addToTaskList(this);
                startLoading();
            }

            @Override
            protected OperationalResult<UserResponse> doInBackground(User... user) {
                UserBusiness userBusiness = new UserBusiness();
                return userBusiness.register(user[0]);
            }


            @Override
            protected void onPostExecute(OperationalResult<UserResponse> userResponse) {
                super.onPostExecute(userResponse);

                if (userResponse.operationalResultResponseType == OperationalResult.OperationalResultResponseType.SUCCESS){
                    callback.onOperationSuccess(userResponse.result);
                }else{
                    callback.onOperationError(userResponse.result, userResponse.error);
                }

                removeToTaskList(this);
                stopLoading();
            }

        }.execute(user);
    }
}
