package sample.myfavoritewine.service.model;

import android.database.Cursor;

import org.json.JSONException;
import org.json.JSONObject;

import sample.myfavoritewine.database.table.WineTable;
import sample.myfavoritewine.infrastructure.LogWine;

/**
 * Created by Monan on 10/15/15.
 */
public class Wine extends JsonManager{

    private static String TAG = Wine.class.getName();

    private static final String JSON_LABEL_NAME = "name";
    private static final String JSON_LABEL_PICTURE = "picture";
    private static final String JSON_LABEL_PRODUCTOR = "productor";
    private static final String JSON_LABEL_ORIGIN = "origin";
    private static final String JSON_LABEL_DISTRIBUTOR = "distributor";
    private static final String JSON_LABEL_IMPORTER = "importer";
    private static final String JSON_LABEL_PRICE = "price";

    private String name;
    private String picture;
    private String productor;
    private String origin;
    private String distributor;
    private String importer;
    private String price;

    public Wine(JSONObject object){
        setName(getString(JSON_LABEL_NAME, object));
        setPicture(getString(JSON_LABEL_PICTURE, object));
        setProductor(getString(JSON_LABEL_PRODUCTOR, object));
        setOrigin(getString(JSON_LABEL_ORIGIN, object));
        setDistributor(getString(JSON_LABEL_DISTRIBUTOR, object));
        setImporter(getString(JSON_LABEL_IMPORTER, object));
        setPrice(getString(JSON_LABEL_PRICE, object));
    }

    public Wine(Cursor cursor){
        setName(cursor.getString(cursor.getColumnIndex(WineTable.NAME)));
        setPicture(cursor.getString(cursor.getColumnIndex(WineTable.PICTURE)));
        setProductor(cursor.getString(cursor.getColumnIndex(WineTable.PRODUCTOR)));
        setOrigin(cursor.getString(cursor.getColumnIndex(WineTable.ORIGIN)));
        setDistributor(cursor.getString(cursor.getColumnIndex(WineTable.DISTRIBUTOR)));
        setImporter(cursor.getString(cursor.getColumnIndex(WineTable.IMPORTER)));
        setPrice(cursor.getString(cursor.getColumnIndex(WineTable.PRICE)));
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getProductor() {
        return productor;
    }

    public void setProductor(String productor) {
        this.productor = productor;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDistributor() {
        return distributor;
    }

    public void setDistributor(String distributor) {
        this.distributor = distributor;
    }

    public String getImporter() {
        return importer;
    }

    public void setImporter(String importer) {
        this.importer = importer;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public JSONObject toJson(){
        JSONObject jsonObject = new JSONObject();
        try{
            jsonObject.put(JSON_LABEL_NAME, getName());
            jsonObject.put(JSON_LABEL_PICTURE, getPicture());
            jsonObject.put(JSON_LABEL_PRODUCTOR, getProductor());
            jsonObject.put(JSON_LABEL_ORIGIN, getOrigin());
            jsonObject.put(JSON_LABEL_DISTRIBUTOR, getDistributor());
            jsonObject.put(JSON_LABEL_IMPORTER, getImporter());
            jsonObject.put(JSON_LABEL_PRICE, getPrice());
        }catch(JSONException e){
            LogWine.e(TAG, "Problemas ao converter objeto para JSON :: " + e.getMessage());
        }
        return jsonObject;
    }
}
