package sample.myfavoritewine.service.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by Monan on 9/8/15.
 */
public class NetworkUtil {

    public static Boolean isConnectionAvailable(Context context){
        Boolean result = Boolean.FALSE;
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if(connectivityManager.getActiveNetworkInfo() != null){
            NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
            result = networkInfo != null && networkInfo.isConnectedOrConnecting();
        }
        return  result;
    }
}
