package sample.myfavoritewine.service.util;

import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.reflect.Array;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import sample.myfavoritewine.infrastructure.LogWine;
import sample.myfavoritewine.service.model.User;
import sample.myfavoritewine.service.model.Wine;

/**
 * Created by Monan on 8/31/15.
 */
public class MongolabManager {

    private static String TAG = MongolabManager.class.getName();

    public static final String API_KEY = "?apiKey=vCGIaC1cfLndci-iH7wxrgvX1q_DO73v";
    public static final String SERVER_URL = "https://api.mongolab.com/api/1/databases";
    public static final String SERVER_BASE_NAME = "/myfavoritewine";
    public static final String COLLECTIONS = "/collections";

    public static final String COLLECTION_USER = "/user";
    public static final String COLLECTION_WINE = "/wine";

    public static final String TYPE_METHOD_POST = "POST";
    public static final String TYPE_METHOD_GET = "GET";

    public List<Wine> getWineList(){
        ArrayList<Wine> result = new ArrayList<Wine>();
        try {
            String URL = SERVER_URL + SERVER_BASE_NAME + COLLECTIONS + COLLECTION_WINE + API_KEY;
            String response = doRequest(URL, TYPE_METHOD_GET, null);
            JSONArray jsonObjectDocument = new JSONArray(response);
            JSONObject jsonObjects = (JSONObject) jsonObjectDocument.get(0);
            JSONArray jsonArray = jsonObjects.getJSONArray("wines");
            for (int i = 0; i < jsonArray.length(); i++){
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                Wine wine = new Wine(jsonObject);
                result.add(wine);
            }
        }catch (JSONException e){
            LogWine.e(TAG, "Problemas ao montar documento de requisição :: " + e.getMessage());
        }
        return result;
    }


    public User registerUser(User user){
        User userResult = null;
        JSONObject document = new JSONObject();
        try{
            String childRequest = "document";
            document.put(childRequest,user.toJson());
            String URL = SERVER_URL + SERVER_BASE_NAME + COLLECTIONS + COLLECTION_USER + API_KEY;
            String response = doRequest(URL, TYPE_METHOD_POST, document.toString());
            JSONObject jsonObjectDocument = new JSONObject(response);
            JSONObject jsonObject = jsonObjectDocument.getJSONObject(childRequest);
            userResult = new User(jsonObject);
        }catch(JSONException e){
            LogWine.e(TAG, "Problemas ao montar documento de requisição :: " + e.getMessage());
        }
        return userResult;
    }

    public String doRequest(String url, String typeMethod, String params){

        String result = null;
        try{
            //Crio e monto minha URL para me comunicar com o servidor
            URL u = new URL(url);

            //Abro uma conexão com o server
            HttpURLConnection conn = (HttpURLConnection) u.openConnection();

            //Defino qual o tipo do request (POST ou GET)
            conn.setRequestMethod(typeMethod);

            //Definindo se teremos ou não escrita no corpo da requisição
            conn.setDoInput(true);
            if (TYPE_METHOD_GET.equals(typeMethod)){
                conn.setDoOutput(false);

                //Realizando a conexão
                conn.connect();
            }else if (TYPE_METHOD_POST.equals(typeMethod)){
                conn.setRequestProperty("Content-Type", "application/json");
                conn.setDoOutput(true);
                //Realizando a conexão
                conn.connect();

                OutputStream out = conn.getOutputStream();
                byte[] bytes = params.getBytes("UTF8");
                out.write(bytes);
                out.flush();
                out.close();
            }

            if(conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                InputStream in = conn.getInputStream();

                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                byte[] buffer = new byte[1024];
                int len;
                while ((len = in.read(buffer)) > 0) {
                    byteArrayOutputStream.write(buffer, 0, len);
                }
                byte[] bytes = byteArrayOutputStream.toByteArray();

                byteArrayOutputStream.close();
                in.close();

                result = new String(bytes);
            }

        }catch(MalformedURLException e){
            LogWine.e(TAG, e.getMessage());
        }catch(IOException e){
            LogWine.e(TAG, e.getMessage());
        }

        return result;
    }
}
