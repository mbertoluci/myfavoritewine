package sample.myfavoritewine.service.util;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.Calendar;

import sample.myfavoritewine.infrastructure.Util;
import sample.myfavoritewine.service.model.UserResponse;

/**
 * Created by Monan on 9/5/15.
 */
public class SessionManager {

    private static SessionManager mInstance;

    private static final String KEY_CREATED_AT = "created_at_key_wine";
    private static final String KEY_NAME = "name_key_wine";
    private static final String KEY_EMAIL = "email_key_wine";
    private static final String KEY_PASSWORD = "password_key_wine";

    private static final String SHARED_PREFERENCE_NAME = "sample.myfavoritewine";

    private SessionManager(){}

    public static SessionManager getInstance(){
        if(mInstance == null){
            mInstance = new SessionManager();
        }
        return  mInstance;
    }

    public void loader(Context context, UserResponse userResponse){
        Calendar calendar = Calendar.getInstance();

        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREFERENCE_NAME, Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putLong(KEY_CREATED_AT, calendar.getTimeInMillis());
        editor.putString(KEY_NAME, Util.encryptDataBase64(userResponse.getName()));
        editor.putString(KEY_EMAIL, Util.encryptDataBase64(userResponse.getEmail()));
        editor.putString(KEY_PASSWORD, userResponse.getPassword());
        editor.apply();
    }

    public String getEmail(Context context){
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREFERENCE_NAME, Context.MODE_PRIVATE);
        return Util.decryptDataBase64(sharedPreferences.getString(KEY_EMAIL, null));
    }

    public String getName(Context context){
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREFERENCE_NAME, Context.MODE_PRIVATE);
        return Util.decryptDataBase64(sharedPreferences.getString(KEY_NAME, null));
    }

    public long getCreatedAt(Context context){
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREFERENCE_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getLong(KEY_CREATED_AT, 0);
    }

    public boolean hasAccess(Context context, String email, String password){
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREFERENCE_NAME, Context.MODE_PRIVATE);
        return (Util.encryptDataBase64(email).equals(sharedPreferences.getString(KEY_EMAIL, "")) &&
                Util.encryptDataBase64(password).equals(sharedPreferences.getString(KEY_PASSWORD, "")));
    }

}
