package sample.myfavoritewine.service.model;

import java.util.Calendar;
import java.util.List;

/**
 * Created by Monan on 9/5/15.
 */
public class WineResponse{

    private long requestAt;
    private List<Wine> wineList;

    public void setRequestAt(){
        Calendar calendar = Calendar.getInstance();
        requestAt = calendar.getTimeInMillis();
    }

    public long getRequestAt(){
        return requestAt;
    }

    public List<Wine> getWineList() {
        return wineList;
    }

    public void setWineList(List<Wine> wineList) {
        this.wineList = wineList;
    }
}
