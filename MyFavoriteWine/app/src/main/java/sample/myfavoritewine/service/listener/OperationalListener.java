package sample.myfavoritewine.service.listener;


import sample.myfavoritewine.service.model.Error;

/**
 * Created by Monan on 9/14/15.
 */
public interface OperationalListener<TResult>{

    public void onOperationSuccess(final TResult result);
    public void onOperationError(final TResult result, Error error);

}
