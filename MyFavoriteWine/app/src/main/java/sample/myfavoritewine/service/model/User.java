package sample.myfavoritewine.service.model;

import org.json.JSONException;
import org.json.JSONObject;

import sample.myfavoritewine.infrastructure.LogWine;
import sample.myfavoritewine.infrastructure.Util;

/**
 * Created by Monan on 8/31/15.
 */
public class User extends JsonManager{

    private static String TAG = User.class.getName();

    public static final int GENDER_MALE = 0;
    public static final int GENDER_FEMALE = 1;

    private static final String JSON_LABEL_NAME = "name";
    private static final String JSON_LABEL_EMAIL = "email";
    private static final String JSON_LABEL_GENDER = "gender";
    private static final String JSON_LABEL_BIRTH_DATE = "birthDate";
    private static final String JSON_LABEL_RECEIVER_MAIL = "receiverMail";
    private static final String JSON_LABEL_PASSWORD = "password";

    private String name;
    private String email;
    private int gender;
    private String birthDate;
    private String receiverMail;
    private String password;

    public User(){}

    public User(JSONObject object){
        setName(getString(JSON_LABEL_NAME, object));
        setEmail(getString(JSON_LABEL_EMAIL, object));
        setGender(getInteger(JSON_LABEL_GENDER, object));
        setBirthDate(getString(JSON_LABEL_BIRTH_DATE, object));
        setPassword(getString(JSON_LABEL_PASSWORD, object), true);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getGender() {
        return gender;
    }

    public String getGenderLabel() {
        return (gender == GENDER_FEMALE) ? "Feminino" : "Masculino";
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getReceiverMail() {
        return receiverMail;
    }

    public void setReceiverMail(boolean receiverMail) {
        this.receiverMail = receiverMail? "SIM":"NÃO";
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password){
        setPassword(password, false);
    }

    public void setPassword(String password, boolean isEncrypt) {
        this.password = isEncrypt? password : Util.encryptDataBase64(password);
    }

    public JSONObject toJson(){
        JSONObject jsonObject = new JSONObject();
        try{
            jsonObject.put(JSON_LABEL_NAME, getName());
            jsonObject.put(JSON_LABEL_EMAIL, getEmail());
            jsonObject.put(JSON_LABEL_GENDER, getGender());
            jsonObject.put(JSON_LABEL_BIRTH_DATE, getBirthDate());
            jsonObject.put(JSON_LABEL_RECEIVER_MAIL, getReceiverMail());
            jsonObject.put(JSON_LABEL_PASSWORD, getPassword());
        }catch(JSONException e){
            LogWine.e(TAG, "Problemas ao converter objeto para JSON :: " + e.getMessage());
        }
        return jsonObject;
    }
}
