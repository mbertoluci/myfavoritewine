package sample.myfavoritewine.service.model;

import android.service.media.MediaBrowserService;

import java.lang.*;

/**
 * Created by Monan on 9/5/15.
 */
public class OperationalResult<TResult> {

    public enum OperationalResultResponseType{
        SUCCESS, ERROR
    }

    public OperationalResultResponseType operationalResultResponseType;
    public TResult result;
    public Error error;
}
