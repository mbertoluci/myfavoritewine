package sample.myfavoritewine.service.manager;

import android.content.Context;
import android.os.AsyncTask;

import java.util.ArrayList;
import java.util.List;

import sample.myfavoritewine.ui.widget.custom.CustomLoadingDialog;

/**
 * Created by Monan on 8/30/15.
 */
public abstract class BaseManager {

    private List<AsyncTask<?,?,?>> asyncTaskList;
    private CustomLoadingDialog mLoadingDialog;

    protected BaseManager(Context context){
        this(context, null);
    }

    protected BaseManager(Context context, CharSequence loadingMessage){
        asyncTaskList = new ArrayList<AsyncTask<?, ?, ?>>();

        if (loadingMessage != null) {
            mLoadingDialog = new CustomLoadingDialog(context);
            mLoadingDialog.setMessage(loadingMessage);
        }
    }

    protected void cancelOperations(){
        for(AsyncTask<?,?,?> task : asyncTaskList){
            task.cancel(true);
        }
    }

    protected void addToTaskList(AsyncTask<?,?,?> task){
        asyncTaskList.add(task);
    }

    protected void removeToTaskList(AsyncTask<?,?,?> task){
        asyncTaskList.remove(task);
    }

    protected void startLoading(){
        if (mLoadingDialog != null)
            mLoadingDialog.show();
    }

    protected void stopLoading(){
        if (mLoadingDialog != null && mLoadingDialog.isShowing())
            mLoadingDialog.dismiss();
    }

}
