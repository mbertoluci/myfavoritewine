package sample.myfavoritewine.service.model;

import java.util.Calendar;

/**
 * Created by Monan on 9/5/15.
 */
public class UserResponse extends User{
    private long requestAt;

    public void setRequestAt(){
        Calendar calendar = Calendar.getInstance();
        requestAt = calendar.getTimeInMillis();
    }

    public long getRequestAt(){
        return requestAt;
    }

}
