package sample.myfavoritewine.service.model;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Monan on 9/20/15.
 */
public abstract class JsonManager {

    protected String getString(String field, JSONObject jsonObject){
        String result = null;
        try {
            if (jsonObject.has(field)) {
                result = jsonObject.getString(field);
            }
        }catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    protected Integer getInteger(String field, JSONObject jsonObject){
        Integer result = null;
        try {
            if (jsonObject.has(field)) {
                result = jsonObject.getInt(field);
            }
        }catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

}
