package sample.myfavoritewine.service.manager;

import android.content.Context;
import android.os.AsyncTask;

import sample.myfavoritewine.R;
import sample.myfavoritewine.service.business.UserBusiness;
import sample.myfavoritewine.service.business.WineBusiness;
import sample.myfavoritewine.service.listener.OperationalListener;
import sample.myfavoritewine.service.model.Error;
import sample.myfavoritewine.service.model.OperationalResult;
import sample.myfavoritewine.service.model.User;
import sample.myfavoritewine.service.model.UserResponse;
import sample.myfavoritewine.service.model.WineResponse;
import sample.myfavoritewine.service.util.NetworkUtil;

/**
 * Created by Monan on 9/5/15.
 */
public class WineManager extends BaseManager{

    private Context mContext;

    public WineManager(Context context) {
        super(context);
        mContext = context;
    }

    public WineManager(Context context, CharSequence loadingMessage) {
        super(context, loadingMessage);
        mContext = context;
    }

    public void getWineList(final OperationalListener<WineResponse> callback){

        if(!NetworkUtil.isConnectionAvailable(mContext)){
            Error error = new Error();
            error.errorMessage = mContext.getResources().getString(R.string.label_problem_with_connection);
            error.errorCode = Error.NO_INTERNET_AVAILABLE;
            callback.onOperationError(null, error);
            return;
        }

        AsyncTask<Void, Void, OperationalResult<WineResponse>> asyncTask = new AsyncTask<Void, Void, OperationalResult<WineResponse>>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                addToTaskList(this);
                startLoading();
            }

            @Override
            protected OperationalResult<WineResponse> doInBackground(Void... params) {
                WineBusiness wineBusiness = new WineBusiness();
                return wineBusiness.getWineList(mContext);
            }


            @Override
            protected void onPostExecute(OperationalResult<WineResponse> wineResponse) {
                super.onPostExecute(wineResponse);

                if (wineResponse.operationalResultResponseType == OperationalResult.OperationalResultResponseType.SUCCESS){
                    callback.onOperationSuccess(wineResponse.result);
                }else{
                    callback.onOperationError(wineResponse.result, wineResponse.error);
                }

                removeToTaskList(this);
                stopLoading();
            }

        }.execute();
    }
}
