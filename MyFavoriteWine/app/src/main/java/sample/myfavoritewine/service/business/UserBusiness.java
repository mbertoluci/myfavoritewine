package sample.myfavoritewine.service.business;

import android.content.Context;

import sample.myfavoritewine.service.model.OperationalResult;
import sample.myfavoritewine.service.manager.BaseManager;
import sample.myfavoritewine.service.model.User;
import sample.myfavoritewine.service.model.UserResponse;
import sample.myfavoritewine.service.util.MongolabManager;

/**
 * Created by Monan on 9/8/15.
 */
public class UserBusiness extends BaseBusiness{

    public UserBusiness(){
        super();
    }

    public OperationalResult<UserResponse> register(User user){

        OperationalResult<UserResponse> result = new OperationalResult<UserResponse>();
        User response = mMongolabManager.registerUser(user);
        if (response != null){
            result.operationalResultResponseType = OperationalResult.OperationalResultResponseType.SUCCESS;

            UserResponse userResponse = new UserResponse();
            userResponse.setName(response.getName());
            userResponse.setBirthDate(response.getBirthDate());
            userResponse.setEmail(response.getEmail());
            userResponse.setPassword(response.getPassword(), true);
            userResponse.setGender(response.getGender());
            userResponse.setRequestAt();
            result.result = userResponse;

        }else{
            result.operationalResultResponseType = OperationalResult.OperationalResultResponseType.ERROR;
        }

        return result;
    }
}
