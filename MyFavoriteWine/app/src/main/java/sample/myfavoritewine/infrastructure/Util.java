package sample.myfavoritewine.infrastructure;

import android.util.Base64;

import java.io.UnsupportedEncodingException;

/**
 * Created by Monan on 8/31/15.
 */
public class Util {

    private static String TAG = Util.class.getName();
    private static String CHARSET_NAME = "UTF-8";

    public static String encryptDataBase64(String value){
        String result = null;
        try{
            if(value != null && !value.isEmpty()){
                byte[] data = value.getBytes(CHARSET_NAME);
                result = Base64.encodeToString(data, Base64.DEFAULT);
            }
        }catch(UnsupportedEncodingException e){
            LogWine.e(TAG, "Problemas durante a criptografia :: " + e.getMessage());
        }
        return result;
    }

    public static String decryptDataBase64(String value){
        String result = null;
        byte[] data = Base64.decode(value, Base64.DEFAULT);
        try{
            result = new String(data, CHARSET_NAME);
        }catch(UnsupportedEncodingException e){
            LogWine.e(TAG, "Problemas durante a descriptografia :: " + e.getMessage());
        }
        return result;
    }

}
