package sample.myfavoritewine.ui.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.util.List;

import sample.myfavoritewine.R;
import sample.myfavoritewine.service.model.Wine;

/**
 * Created by Monan on 10/17/15.
 */
public class WineListAdapter extends BaseAdapter{

    private Context mContext;
    private List<Wine> mWineList;

    public WineListAdapter(Context context, List<Wine> wineList){
        mContext = context;
        mWineList = wineList;
    }

    @Override
    public int getCount() {
        return mWineList.size();
    }

    @Override
    public Object getItem(int position) {
        return mWineList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder = null;
        LayoutInflater mInflater = (LayoutInflater) mContext.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if(convertView == null){
            convertView = mInflater.inflate(R.layout.custom_item_list_wine, null);
            viewHolder = new ViewHolder();
            viewHolder.imageViewBackground = (ImageView) convertView.findViewById(R.id.imageview_background);
            viewHolder.textViewName = (TextView) convertView.findViewById(R.id.textview_name);
            viewHolder.textViewCreatedBy = (TextView) convertView.findViewById(R.id.textview_created_by);
            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder) convertView.getTag();
        }


        Wine wine = mWineList.get(position);

        Picasso.with(mContext)
                .load(wine.getPicture())
                .placeholder(R.drawable.place_holder)
                .into(viewHolder.imageViewBackground);

        viewHolder.textViewName.setText(wine.getName());
        viewHolder.textViewCreatedBy.setText(wine.getProductor());

        return convertView;
    }

    private class ViewHolder{
        ImageView imageViewBackground;
        TextView textViewName;
        TextView textViewCreatedBy;
    }
}
