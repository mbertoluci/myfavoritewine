package sample.myfavoritewine.ui.widget.custom;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.widget.DatePicker;

import java.util.Calendar;

/**
 * Created by Monan on 8/23/15.
 */
public class GenderDatePickerDialog extends DialogFragment implements DatePickerDialog.OnDateSetListener, DatePickerDialog.OnDismissListener{

    private DatePickerDialog.OnDateSetListener mCallbackDataSet;
    private DatePickerDialog.OnDismissListener mCallbackOnDismiss;


    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        final Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH)+1;
        int day = cal.get(Calendar.DAY_OF_MONTH);

        return new DatePickerDialog(getActivity(), this, year, month, day);
    }


    public void setOnDateSetListener(DatePickerDialog.OnDateSetListener callback){
        mCallbackDataSet = callback;
    }

    public void setOnDismissListener(DatePickerDialog.OnDismissListener callback){
        mCallbackOnDismiss = callback;
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        if (mCallbackDataSet != null){
            mCallbackDataSet.onDateSet(view, year, monthOfYear, dayOfMonth);
        }
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        if (mCallbackOnDismiss != null){
            mCallbackOnDismiss.onDismiss(dialog);
        }
    }
}
