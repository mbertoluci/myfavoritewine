package sample.myfavoritewine.ui.widget.custom;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import sample.myfavoritewine.R;

/**
 * Created by Monan on 9/5/15.
 */
public class CustomAlertDialog extends Dialog{

    public static final int DIALOG_TYPE_NOTIFICATION = 1;
    public static final int DIALOG_TYPE_CONFIRM = 2;

    private TextView mTextViewTitle;
    private TextView mTextViewMessage;
    private Button mButtonCancel;
    private Button mButtonConfirm;

    public CustomAlertDialog(Context context, int typeDialog) {
        super(context, android.R.style.Theme_Translucent_NoTitleBar);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.widget_custom_alert_dialog);

        mTextViewTitle = (TextView) findViewById(R.id.text_view_title);
        mTextViewMessage = (TextView) findViewById(R.id.text_view_message);
        mButtonCancel = (Button) findViewById(R.id.button_cancel);
        mButtonConfirm = (Button) findViewById(R.id.button_confirm);

        if (typeDialog == DIALOG_TYPE_NOTIFICATION){
            setDialogTypeNotification();
        }else{
            setDialogTypeConfirm();
        }
    }

    public void setOnClickListenerConfirmButton(View.OnClickListener onClickListener){
        mButtonConfirm.setOnClickListener(onClickListener);
    }

    public void setOnClickListenerCancelButton(View.OnClickListener onClickListener){
        mButtonCancel.setOnClickListener(onClickListener);
    }

    public void setTitle(String title){
        mTextViewTitle.setText(title);
    }

    public void setMessage(String message){
        mTextViewMessage.setText(message);
    }

    private void setDialogTypeConfirm(){
        mButtonConfirm.setText(getContext().getResources().getString(R.string.label_button_message_continue));
        mButtonConfirm.setVisibility(View.VISIBLE);
        mButtonCancel.setVisibility(View.VISIBLE);
    }

    private void setDialogTypeNotification(){
        mButtonConfirm.setText(getContext().getResources().getString(R.string.label_button_message_understand));
        mButtonConfirm.setVisibility(View.VISIBLE);
        mButtonCancel.setVisibility(View.GONE);
    }

}
