package sample.myfavoritewine.ui.fragment;

import android.app.Activity;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import sample.myfavoritewine.R;
import sample.myfavoritewine.infrastructure.Constants;
import sample.myfavoritewine.service.listener.OperationalListener;
import sample.myfavoritewine.service.manager.UserManager;
import sample.myfavoritewine.service.manager.WineManager;
import sample.myfavoritewine.service.model.*;
import sample.myfavoritewine.service.util.SessionManager;
import sample.myfavoritewine.ui.activity.LoginActivity;
import sample.myfavoritewine.ui.activity.RegisterActivity;
import sample.myfavoritewine.ui.activity.WineListActivity;
import sample.myfavoritewine.ui.adapter.WineListAdapter;
import sample.myfavoritewine.ui.widget.custom.CustomAlertDialog;

/**
 * Created by Monan on 8/22/15.
 */
public class WineListFragment extends Fragment{


    private WineListActivity mWineListActivity;
    private ListView mListViewWine;
    private WineListAdapter mWineListAdapter;
    private List<Wine> mListWine;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_wine_list, null, false);
        mListViewWine = (ListView) view.findViewById(R.id.listview_wine);

        mListWine = new ArrayList<Wine>();
        mWineListAdapter = new WineListAdapter(mWineListActivity, mListWine);
        mListViewWine.setAdapter(mWineListAdapter);

        WineManager wineManager = new WineManager(mWineListActivity, mWineListActivity.getResources().getString(R.string.label_dialog_message_register_message));
        wineManager.getWineList(new OperationalListener<WineResponse>() {
            @Override
            public void onOperationSuccess(WineResponse wineResponse) {
                mListWine.clear();
                mListWine.addAll(wineResponse.getWineList());
                mWineListAdapter.notifyDataSetChanged();
            }

            @Override
            public void onOperationError(WineResponse wineResponse, sample.myfavoritewine.service.model.Error error) {
                showDialogNotification(error.errorMessage);
            }
        });

        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mWineListActivity = (WineListActivity) activity;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        configureToolbar();
    }


    private void configureToolbar(){
        Toolbar toolbar = (Toolbar) mWineListActivity.findViewById(R.id.toolbar);
        mWineListActivity.setSupportActionBar(toolbar);
        mWineListActivity.getSupportActionBar().setTitle(R.string.label_toolbar_wine_screen_title);
        mWineListActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        mWineListActivity.getSupportActionBar().setDisplayShowHomeEnabled(false);
    }

    private void showDialogNotification(String message){
        final CustomAlertDialog alertDialog = new CustomAlertDialog(mWineListActivity, CustomAlertDialog.DIALOG_TYPE_NOTIFICATION);
        alertDialog.setTitle(mWineListActivity.getResources().getString(R.string.label_toolbar_wine_screen_title));
        alertDialog.setMessage(message);
        alertDialog.setOnClickListenerConfirmButton(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }
}
