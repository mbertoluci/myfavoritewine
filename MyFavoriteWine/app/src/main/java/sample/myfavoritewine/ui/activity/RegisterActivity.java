package sample.myfavoritewine.ui.activity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import sample.myfavoritewine.R;
import sample.myfavoritewine.ui.fragment.RegisterFragment;

/**
 * Created by Monan on 8/23/15.
 */
public class RegisterActivity extends AppCompatActivity {

    @SuppressLint("MissingSuperCall")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_content);

        RegisterFragment registerFragment = new RegisterFragment();
        getSupportFragmentManager().beginTransaction().
                add(R.id.fragment_content, registerFragment).
                commit();
    }
}
