package sample.myfavoritewine.ui.widget.custom;

import android.app.Dialog;
import android.content.Context;
import android.view.Window;
import android.widget.TextView;

import sample.myfavoritewine.R;

/**
 * Created by Monan on 8/31/15.
 */
public class CustomLoadingDialog extends Dialog{

    private TextView mTextViewLoadingMessage;

    public CustomLoadingDialog(Context context) {
        super(context, android.R.style.Theme_Translucent_NoTitleBar);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.widget_custom_dialog_progress);

        mTextViewLoadingMessage = (TextView) findViewById(R.id.text_view_loading_message);
    }

    public void setMessage(CharSequence message){
        mTextViewLoadingMessage.setText(message);
    }

    public  void setTextViewLoadingMessageVisibility(int visibility){
        mTextViewLoadingMessage.setVisibility(visibility);
    }
}
