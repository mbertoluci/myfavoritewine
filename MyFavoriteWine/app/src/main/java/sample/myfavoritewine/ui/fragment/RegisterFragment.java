package sample.myfavoritewine.ui.fragment;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

import sample.myfavoritewine.R;
import sample.myfavoritewine.infrastructure.Constants;
import sample.myfavoritewine.service.listener.OperationalListener;
import sample.myfavoritewine.service.model.*;
import sample.myfavoritewine.service.manager.UserManager;
import sample.myfavoritewine.service.util.SessionManager;
import sample.myfavoritewine.ui.activity.RegisterActivity;
import sample.myfavoritewine.ui.widget.custom.CustomAlertDialog;
import sample.myfavoritewine.ui.widget.custom.GenderDatePickerDialog;

/**
 * Created by Monan on 8/22/15.
 */
public class RegisterFragment extends Fragment{

    private RegisterActivity mRegisterActivity;

    private EditText mEditTextName;
    private EditText mEditTextEmail;
    private RadioGroup mRadioGroupGender;
    private EditText mEditTextActionDatePicker;
    private CheckBox mCheckBoxPromotional;
    private EditText mEditTextPassword;
    private EditText mEditTextConfirmPassword;
    private Button mButtonLogin;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_register, null, false);

        mEditTextName = (EditText) view.findViewById(R.id.edit_text_name);
        mEditTextEmail = (EditText) view.findViewById(R.id.edit_text_email);
        mEditTextPassword = (EditText) view.findViewById(R.id.edit_text_password);
        mEditTextConfirmPassword = (EditText) view.findViewById(R.id.edit_text_confirm_password);

        mEditTextActionDatePicker = (EditText) view.findViewById(R.id.edit_text_action_date_picker);
        mEditTextActionDatePicker.setOnFocusChangeListener(onFocusChangeListenerGender);

        mRadioGroupGender = (RadioGroup) view.findViewById(R.id.radio_group_gender);
        mCheckBoxPromotional = (CheckBox) view.findViewById(R.id.check_box_receiver_promotional);

        mButtonLogin = (Button) view.findViewById(R.id.button_login);
        mButtonLogin.setOnClickListener(onClickListener);

        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mRegisterActivity = (RegisterActivity) activity;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        configureToolbar();
    }

    private void configureToolbar(){
        Toolbar toolbar = (Toolbar) mRegisterActivity.findViewById(R.id.toolbar);
        mRegisterActivity.setSupportActionBar(toolbar);

        mRegisterActivity.getSupportActionBar().setTitle(R.string.label_toolbar_register_screen_title);
        mRegisterActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mRegisterActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);

        final Drawable upArrowIcon = ContextCompat.getDrawable(mRegisterActivity, R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        upArrowIcon.setColorFilter(mRegisterActivity.getResources().getColor(R.color.material_white_color), PorterDuff.Mode.SRC_ATOP);
        mRegisterActivity.getSupportActionBar().setHomeAsUpIndicator(upArrowIcon);


        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mRegisterActivity.finish();
            }
        });
    }

    private void showDialogNotification(String message){
        final CustomAlertDialog alertDialog = new CustomAlertDialog(mRegisterActivity, CustomAlertDialog.DIALOG_TYPE_NOTIFICATION);
        alertDialog.setTitle(mRegisterActivity.getResources().getString(R.string.label_title_dialog_fragment));
        alertDialog.setMessage(message);
        alertDialog.setOnClickListenerConfirmButton(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }

    /**
     * Listeners
     */

    private View.OnClickListener onClickListener = new View.OnClickListener(){
        @Override
        public void onClick(View v) {

            boolean resultForms;
            resultForms = isRequiredField(mEditTextName);
            resultForms = isRequiredField(mEditTextEmail);
            resultForms = isRequiredField(mEditTextActionDatePicker);
            resultForms = isRequiredField(mEditTextPassword);
            resultForms = isValidEmail(mEditTextEmail);
            resultForms = wasConfirmPassword(mEditTextPassword, mEditTextConfirmPassword);

            if (resultForms){
                final User user = new User();
                user.setName(mEditTextName.getText().toString());
                user.setEmail(mEditTextEmail.getText().toString());
                user.setBirthDate(mEditTextActionDatePicker.getText().toString());
                int gender = (mRadioGroupGender.getCheckedRadioButtonId() == R.id.gender_option_male)? User.GENDER_MALE : User.GENDER_FEMALE;
                user.setGender(gender);
                user.setPassword(mEditTextPassword.getText().toString());
                user.setReceiverMail(mCheckBoxPromotional.isChecked());

                UserManager userManager = new UserManager(mRegisterActivity, mRegisterActivity.getResources().getString(R.string.label_dialog_message_register_message));
                userManager.register(user, new OperationalListener<UserResponse>() {
                    @Override
                    public void onOperationSuccess(UserResponse userResponse) {
                        SessionManager sessionManager = SessionManager.getInstance();
                        sessionManager.loader(mRegisterActivity, userResponse);

                        Toast.makeText(mRegisterActivity, mRegisterActivity.getResources().getString(R.string.toast_label_register_done), Toast.LENGTH_LONG).show();
                        Intent it = new Intent();
                        it.putExtra(Constants.BundleKeys.LoginActivity.BUNDLE_EMAIL, userResponse.getEmail());
                        mRegisterActivity.setResult(Activity.RESULT_OK, it);
                        mRegisterActivity.finish();
                    }

                    @Override
                    public void onOperationError(UserResponse userResponse, sample.myfavoritewine.service.model.Error error) {
                        showDialogNotification(error.errorMessage);
                    }
                });
            }
        }
    };

    private boolean isRequiredField(EditText editText){
        boolean result = true;
        if (editText.getText().toString().isEmpty()){
            editText.setError(mRegisterActivity.getString(R.string.label_error_message_field_required));
            result = false;
        }

        return result;
    }

    private boolean isValidEmail(EditText editText){
        boolean result = true;
        if ((editText.getText().toString().isEmpty()) || (!Patterns.EMAIL_ADDRESS.matcher(editText.getText()).matches())){
            result = false;
            editText.setError(mRegisterActivity.getString(R.string.label_error_message_invalid_email));
        }
        return result;
    }

    private boolean wasConfirmPassword(EditText editTextPassword, EditText editTextConfirmPassword){
        boolean result = false;
        if (!editTextConfirmPassword.getText().toString().isEmpty()){
            if (editTextPassword.getText().toString().equals(editTextConfirmPassword.getText().toString())){
                result = true;
            }
        }
        if (!result){
            editTextConfirmPassword.setError(mRegisterActivity.getResources().getString(R.string.label_error_message_confirm_password));
        }
        return result;
    }

    private View.OnFocusChangeListener onFocusChangeListenerGender = new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            if (hasFocus){
                GenderDatePickerDialog genderDatePickerDialog = new GenderDatePickerDialog();
                genderDatePickerDialog.setOnDateSetListener(onDateSetListener);
                genderDatePickerDialog.setOnDismissListener(onDismissListener);
                genderDatePickerDialog.show(getFragmentManager(), getResources().getString(R.string.label_hint_register_birthdate));
            }
        }
    };

    private DatePickerDialog.OnDateSetListener onDateSetListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            String dateSelected = dayOfMonth + "/" + monthOfYear + "/" + year;
            mEditTextActionDatePicker.setText(dateSelected);
        }
    };

    private DatePickerDialog.OnDismissListener onDismissListener = new DatePickerDialog.OnDismissListener(){
        @Override
        public void onDismiss(DialogInterface dialog) {
            mEditTextActionDatePicker.setError(null);
            mEditTextPassword.requestFocus();
        }
    };
}
