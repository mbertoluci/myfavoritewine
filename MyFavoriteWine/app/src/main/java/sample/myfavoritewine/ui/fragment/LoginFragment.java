package sample.myfavoritewine.ui.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import sample.myfavoritewine.R;
import sample.myfavoritewine.infrastructure.Constants;
import sample.myfavoritewine.service.util.SessionManager;
import sample.myfavoritewine.ui.activity.LoginActivity;
import sample.myfavoritewine.ui.activity.RegisterActivity;
import sample.myfavoritewine.ui.activity.WineListActivity;
import sample.myfavoritewine.ui.widget.custom.CustomAlertDialog;

/**
 * Created by Monan on 8/22/15.
 */
public class LoginFragment extends Fragment{

    private static final int CODE_REQUEST_INTENT_REGISTER = 1;

    private LoginActivity mLoginActivity;

    private EditText mEditTextLogin;
    private EditText mEditTextPassword;
    private Button mButtonLogin;
    private TextView mTextViewRegister;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, null, false);

        mEditTextLogin = (EditText) view.findViewById(R.id.edit_text_login);
        mEditTextPassword = (EditText) view.findViewById(R.id.edit_text_password);
        mButtonLogin = (Button) view.findViewById(R.id.button_login);
        mTextViewRegister = (TextView) view.findViewById(R.id.text_view_register);

        mButtonLogin.setOnClickListener(onClickListener);
        mTextViewRegister.setOnClickListener(onClickListener);

        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mLoginActivity = (LoginActivity) activity;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        configureToolbar();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK){
            switch(requestCode){
                case CODE_REQUEST_INTENT_REGISTER:
                    if (data != null) {
                        String email = data.getStringExtra(Constants.BundleKeys.LoginActivity.BUNDLE_EMAIL);
                        mEditTextLogin.setText(email);
                    }
                    break;
            }
        }
    }

    private void configureToolbar(){
        Toolbar toolbar = (Toolbar) mLoginActivity.findViewById(R.id.toolbar);
        toolbar.setVisibility(View.GONE);
    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch(v.getId()){
                case R.id.text_view_register:
                    Intent it = new Intent(mLoginActivity, RegisterActivity.class);
                    startActivityForResult(it, CODE_REQUEST_INTENT_REGISTER);
                    break;
                case R.id.button_login:

                    String email = mEditTextLogin.getText().toString();
                    String password = mEditTextPassword.getText().toString();

                    Intent intent = new Intent(mLoginActivity, WineListActivity.class);
                    startActivity(intent);
                    mLoginActivity.finish();

//                    SessionManager sessionManager = SessionManager.getInstance();
//                    if (sessionManager.hasAccess(mLoginActivity, email, password)){
//
//                    }else{
//                        Toast.makeText(mLoginActivity, "Erro", Toast.LENGTH_LONG).show();
//                    }

                    break;
            }
        }
    };
}
