package sample.myfavoritewine.ui.widget.custom;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.widget.ProgressBar;

import sample.myfavoritewine.R;

/**
 * Created by Monan on 8/31/15.
 */
public class CustomProgressBar extends ProgressBar{

    private RectF mRectF;
    private Paint mPaint;
    private int mStart;
    private int mMaxValue;
    private int mValue;
    private boolean mReverse;

    public CustomProgressBar(Context context) {
        super(context);
        init();
    }

    public CustomProgressBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomProgressBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init(){
        mStart = 0;
        mMaxValue = 320;
        mValue = 320;
        mReverse = false;

        mPaint = new Paint();
        mPaint.setStrokeWidth(10);
        mPaint.setStrokeCap(Paint.Cap.ROUND);
        mPaint.setAntiAlias(true);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setColor(getResources().getColor(R.color.progress_bar_loading_color));
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        mRectF = new RectF(0+5, 0+5, w-5, h-5);
    }

    @Override
    protected synchronized void onDraw(Canvas canvas) {
        if(mReverse){
            mStart +=15;
            mValue += 10;
        }else{
            mStart += 5;
            mValue -= 10;
        }

        if (mStart == 360){
            mStart = 1;
        }

        if(mValue == 0 || mValue == mMaxValue){
            mReverse = !mReverse;
        }

        mPaint.setColor(getResources().getColor(R.color.progress_bar_loading_color));
        canvas.drawArc(mRectF, mStart, mMaxValue - mValue, false, mPaint);
        invalidate();
    }
}
